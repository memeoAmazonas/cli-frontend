export const REGEX_EMAIL = /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/;

export const validateInput = (value, pattern, multiple = false) => {
  if (!multiple) {
    return !pattern.test(value);
  }
  // eslint-disable-next-line no-restricted-syntax
  for (const item of pattern) {
    if (!item.test(value)) {
      return false;
    }
  }
  return true;
};
