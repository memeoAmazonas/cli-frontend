import React from 'react';

export const ApiOptionContext = React.createContext();

export const ApiContextProvider = (props) => {
  const [options, setOptions] = React.useState();
  const { children } = props;
  return (
    <ApiOptionContext.Provider value={[options, setOptions]}>
      {children}
    </ApiOptionContext.Provider>
  );
};
