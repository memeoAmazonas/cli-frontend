import styled from 'styled-components';
import Card from '@material-ui/core/Card';

const CustomCard = styled(Card)((props) => (`
    &.MuiCard-root {
      align-items: center;
      display: ${props.display || 'block'};
      flex-direction: column;
      height: ${props.height || 'inherit'};
      max-height: ${props.maxheight || 'inherit'};
      justify-content: center;
      padding: ${props.padding || '0'};
      width: ${props.width || 'inherit'};
      max-width:${props.maxwidth || 'inherit'};
      margin-right: ${props.marginr || '0'};
      margin-left: ${props.marginl || '0'};
      margin-top: ${props.margint || '0'};
      margin-bottom: ${props.marginb || '0'};
    }
  `));
export default CustomCard;
