import styled from 'styled-components';
import Container from '@material-ui/core/Container';

export const CustomContainer = styled(Container)`
  &.MuiContainer-root{
    align-items: center;
    display: flex;
    justify-content: center;
    min-height: calc(100vh - 64px);
  }
`;
export const ContentAlt = styled.div`
  margin-top: 20px;
  width: 100%;
  display: flex;
`;

export const ViewContainer = styled(Container)((props) => (`
  &.MuiContainer-root{
    padding-top: 20px;
    max-width:100%;
    display: flex;
    flex-wrap:wrap;
    justify-content: ${props.jc || 'space-between'};
  }
`));
