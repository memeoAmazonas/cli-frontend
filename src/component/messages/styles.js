import makeStyles from '@material-ui/core/styles/makeStyles';

const messagesClass = makeStyles((theme) => ({
  titleNotLogged: {
    color: theme.palette.blue2,
    fontWeight: 'bold',
    fontSize: '23',
  },
  titleLogged: {
    color: theme.palette.blue1,
    fontSize: '26',
    fontWeight: 'bold',
    textTransform: 'uppercase',
  },
  languageField: {
    color: theme.palette.blue1,
    fontWeight: 'bold',
    marginLeft: 5,
  },
  titleOption: {
    color: theme.palette.blue1,
    fontSize: 22,
  },
  titleOptionApi: {
    color: theme.palette.blue1,
    fontSize: 18,
    textAlign: 'justify',
  },
  subTitleOption: {
    color: theme.palette.grey3,
    fontSize: 18,
    marginBottom: 10,
    marginTop: 10,
    wordBreak: 'break-all',
    paddingRight: 5,
  },
  mrR5: {
    marginRight: 10,
  },
  font20: {
    fontSize: 20,
  },
  detailTitle: {
    color: theme.palette.blue1,
    fontSize: 18,
    textTransform: 'uppercase',
    fontWeight: 'bold',
    wordBreak: 'break-all',
    paddingRight: 5,
  },
  contentLabelDetail: {
    width: 220,
    marginRight: 10,
  },
  contentDetailUrlHub: {
    width: '100%',
  },
  ul: {
    marginTop: 0,
    paddingLeft: 20,
  },
  li: {
    color: theme.palette.blue1,
    fontSize: 20,
  },
}));
export default messagesClass;
