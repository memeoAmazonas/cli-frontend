import React from 'react';

import Font from './Font';
import messagesClass from './styles';

const LabelDetail = ({
  label, valuesLabel = {}, detail, isArray = false, className = null,
}) => {
  const classes = messagesClass();
  const classname = className ? classes[className] : classes.contentLabelDetail;
  const content = isArray
    ? detail.map((it) => (
      <li
        key={it}
        className={classes.li}
      >
        <Font id={it} className="subTitleOption" style={{ marginTop: 0 }} />
      </li>
    ))
    : <li className={classes.li}><Font id={detail} className="subTitleOption" style={{ marginTop: 0 }} /></li>;
  return (
    <div className={classname}>
      <Font id={label} values={valuesLabel} className="detailTitle" />
      <ul className={classes.ul}>{content}</ul>
    </div>
  );
};
export default LabelDetail;
