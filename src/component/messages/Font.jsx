import React from 'react';
import Typography from '@material-ui/core/Typography';
import { useTranslation } from 'react-i18next';

import messagesClass from 'component/messages/styles';

const Font = ({
  id = 'empty', values = {}, rest = { variant: 'body1', component: 'span' }, className = null, style = {},
}) => {
  const { t } = useTranslation();
  const clasess = messagesClass();
  const classN = className ? clasess[className] : '';
  const result = () => {
    let res = id;
    try {
      res = t(id, values);
    } catch (e) {
      res = id;
    }
    return res;
  };
  return (
    <Typography {...rest} className={classN} style={style}>
      {result()}
    </Typography>
  );
};
export default Font;
