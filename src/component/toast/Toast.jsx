import React from 'react';
import Snackbar from '@material-ui/core/Snackbar';
import Alert from '@material-ui/lab/Alert';
import Font from 'component/messages/Font';

const Toast = ({ open, message, severity = 'success' }) => (
  <div>
    <Snackbar
      anchorOrigin={{
        vertical: 'top',
        horizontal: 'left',
      }}
      open={open}
      autoHideDuration={6000}
      severity="success"
    >
      <Alert severity={severity}><Font id={message} /></Alert>
    </Snackbar>
  </div>
);

export default Toast;
