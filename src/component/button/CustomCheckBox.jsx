import React from 'react';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Font from 'component/messages/Font';

const CustomCheckBox = ({
  checked = false, name, onChange, label,
}) => (
  <FormControlLabel
    control={(
      <Checkbox
        checked={checked}
        name={name}
        onChange={onChange}
        color="primary"
      />
            )}
    label={<Font id={label} />}
  />
);

export default CustomCheckBox;
