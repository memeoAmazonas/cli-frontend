import React from 'react';
import FormLabel from '@material-ui/core/FormLabel';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControl from '@material-ui/core/FormControl';
import Font from 'component/messages/Font';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Radio from '@material-ui/core/Radio';

const CustomRadioButton = ({
  data, value, onChange, title, keyValue = null, keyLabel = null,
  className = '', valuesTitle = {},
}) => {
  const items = data.map((it) => (
    <FormControlLabel
      aria-label={!keyLabel ? it : it[keyLabel]}
      control={<Radio color="primary" />}
      key={!keyValue ? it : it[keyValue]}
      label={<Font id={!keyLabel ? it : it[keyLabel]} />}
      value={!keyValue ? it : it[keyValue]}
    />
  ));
  return (
    <FormControl component="fieldset" className={className}>
      <FormLabel component="legend"><Font id={title} values={valuesTitle} className="titleOptionApi" /></FormLabel>
      <RadioGroup
        arial-label={`radio-group-${title}`}
        value={value || ''}
        onChange={onChange}
      >
        {items}
      </RadioGroup>
    </FormControl>
  );
};
export default CustomRadioButton;
