import React from 'react';
import IdleTimer from 'react-idle-timer';
import { useDispatch } from 'react-redux';
import { ActionAsync } from 'redux/action';
import { useHistory } from 'react-router-dom';
import { LOGOUT } from 'redux/types';

const ExpireSession = () => {
  const idleTimerRef = React.useRef(null);
  const history = useHistory();
  const dispatch = useDispatch();
  const onHidle = () => {
    dispatch(ActionAsync(LOGOUT));
    history.push('/');
  };
  return (
    <div>
      <IdleTimer
        ref={idleTimerRef}
        timeout={2000000}
        onIdle={onHidle}
      />
    </div>
  );
};

export default ExpireSession;
