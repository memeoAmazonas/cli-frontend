import React from 'react';

const WhiteIcon = (Icon) => (<Icon htmlColor="#fff" />);

export default WhiteIcon;
