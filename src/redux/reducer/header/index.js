import { SET_LANGUAGE, SET_TITLE_PAGE } from 'redux/types';

const header = (state = { title: 'name.app' }, { type, payload }) => {
  const response = {
    [SET_LANGUAGE]: { ...state, language: payload },
    [SET_TITLE_PAGE]: { ...state, title: payload },
  };
  return response[type] || { ...state };
};

export default header;
