import { LOGIN, LOGIN_ERROR, LOGIN_SUCCESS } from 'redux/types';

const login = (state = {}, { type, payload }) => {
  const result = {
    [LOGIN]: { ...state, loading: true },
    [LOGIN_SUCCESS]: { ...state, loading: false, data: payload },
    [LOGIN_ERROR]: { ...state, loading: false, error: payload },
  };
  return result[type] || { ...state };
};
export default login;
