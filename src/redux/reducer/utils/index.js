import { LOADING } from 'redux/types';

const utils = (state = {}, { type, payload }) => {
  if (type === LOADING) {
    return { ...state, loading: payload };
  }
  return { ...state };
};
export default utils;
