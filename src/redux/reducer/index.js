import { combineReducers } from 'redux';
import { LOGOUT } from '../types';

import { cli, createProject, getTemplates } from './cli';
import header from './header';
import user from './user';
import utils from './utils';

const appReducers = combineReducers({
  cli,
  createProject,
  getTemplates,
  header,
  user,
  utils,
});

const rootReducer = (state, action) => {
  let nextState = state;
  if (action.type === LOGOUT) {
    nextState = undefined;
    sessionStorage.clear();
  }
  return appReducers(nextState, action);
};

export default rootReducer;
