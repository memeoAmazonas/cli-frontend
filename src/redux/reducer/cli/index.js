import * as TYPE from 'redux/types';

export const cli = (state = {}, { type, payload }) => {
  const result = {
    [TYPE.GET_LIST]: { loading: true },
    [TYPE.GET_LIST_SUCCESS]: { loading: false, data: payload },
    [TYPE.GET_LIST_ERROR]: { loading: false, errorListsh: payload },
    [TYPE.SELECT_TYPE_API]: { type: payload },
    [TYPE.DOWNLOAD_SUCCESS]: { project: payload },
    [TYPE.DOWNLOAD_ERROR]: { err: payload },
    [TYPE.CREATE_SUCCESS]: { createPro: payload },
    [TYPE.CREATE_ERROR]: { createProErr: payload },
    [TYPE.GET_GROUP]: { loadingGroup: true },
    [TYPE.GET_GROUP_SUCCESS]: { loadingGroup: false, group: payload },
    [TYPE.GET_GROUP_ERROR]: { loadingGroup: false, errGroup: payload },
  };
  return { ...state, ...result[type] } || { ...state };
};
export const createProject = (state = {}, { type, payload }) => {
  const result = {
    [TYPE.CREATE_INTEGRATION]: { loading: true },
    [TYPE.CREATE_INTEGRATION_SUCCESS]: { loading: false, data: payload },
    [TYPE.CREATE_INTEGRATION_ERROR]: { loading: false, error: payload },
  };
  return { ...state, ...result[type] } || { ...state };
};

export const getTemplates = (state = {}, { type, payload }) => {
  const result = {
    [TYPE.GET_TEMPLATE]: { loading: true },
    [TYPE.GET_TEMPLATE_SUCCESS]: { loading: false, templates: payload },
    [TYPE.GET_TEMPLATE_ERROR]: { loading: false, error: payload },
  };
  return { ...state, ...result[type] } || { ...state };
};
