import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
// eslint-disable-next-line import/no-extraneous-dependencies
import { createLogger } from 'redux-logger';
import createSagaMiddleware from 'redux-saga';
import rootSaga from 'redux/api/sagas';
import reducer from 'redux/reducer';

const configureStore = () => {
  const logger = createLogger();
  const sagaMiddleware = createSagaMiddleware();
  return {
    ...createStore(
      reducer,
      applyMiddleware(
        sagaMiddleware,
        thunk,
        logger,
      ),
    ),
    runSaga: sagaMiddleware.run(rootSaga),
  };
};
export default configureStore;
