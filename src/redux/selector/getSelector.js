import get from 'lodash/get';
import { useSelector } from 'react-redux';

const getKey = (store, key) => get(store, key);

const GetRedux = (key) => useSelector((state) => getKey(state, key));

export default GetRedux;
