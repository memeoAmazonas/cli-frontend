export const SET_LANGUAGE = 'set_language';
export const SET_TITLE_PAGE = 'set_title_page';

export const LOGOUT = 'logout';
export const LOGIN = 'login';
export const LOGIN_SUCCESS = 'login_success';
export const LOGIN_ERROR = 'login_error';

export const GET_LIST = 'get_list';
export const GET_LIST_SUCCESS = 'get_list_success';
export const GET_LIST_ERROR = 'get_list_error';
export const SELECT_TYPE_API = 'select_type_api';

export const CREATE = 'create';
export const CREATE_SUCCESS = 'create_success';
export const CREATE_ERROR = 'create_error';

export const CREATE_INTEGRATION = 'create_integration';
export const CREATE_INTEGRATION_SUCCESS = 'create_integration_success';
export const CREATE_INTEGRATION_ERROR = 'create_integration_error';

export const DOWNLOAD = 'download';
export const DOWNLOAD_SUCCESS = 'download_error';
export const DOWNLOAD_ERROR = 'download_error';

export const GET_GROUP = 'get_group';
export const GET_GROUP_SUCCESS = 'get_group_success';
export const GET_GROUP_ERROR = 'get_group_error';

export const GET_TEMPLATE = 'get_template';
export const GET_TEMPLATE_SUCCESS = 'get_template_success';
export const GET_TEMPLATE_ERROR = 'get_template_error';

export const LOADING = 'loading';
