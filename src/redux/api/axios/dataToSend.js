import * as TYPE from 'redux/types';
import * as url from './url';

const method = {
  POST: 'POST',
  GET: 'GET',
};

export const LOGIN_TO_SEND = {
  success: TYPE.LOGIN_SUCCESS,
  error: TYPE.LOGIN_ERROR,
  url: url.LOGIN_URL,
  method: method.POST,
};

export const LIST_TO_SEND = {
  success: TYPE.GET_LIST_SUCCESS,
  error: TYPE.GET_LIST_ERROR,
  url: url.LIST_URL,
  method: method.GET,
};

export const CREATE_PROJECT = {
  success: TYPE.CREATE_SUCCESS,
  error: TYPE.CREATE_ERROR,
  url: url.CREATE_PROJECT,
  method: method.POST,
};
export const CREATE_PROJECT_INTEGRATION = {
  success: TYPE.CREATE_SUCCESS,
  error: TYPE.CREATE_ERROR,
  url: url.CREATE_PROJECT_INTEGRATION,
  method: method.POST,
};

export const GET_GROUP_TO_SEND = {
  success: TYPE.GET_GROUP_SUCCESS,
  error: TYPE.GET_GROUP_ERROR,
  url: url.GROUPS_URL,
  method: method.GET,
};
export const GET_TEMPLATES_TO_SEND = {
  success: TYPE.GET_TEMPLATE_SUCCESS,
  error: TYPE.GET_TEMPLATE_ERROR,
  url: url.GET_TEMPLATES,
  method: method.GET,
};
