import axios from 'axios';

const SITE_API = process.env.REACT_APP_API_BASE;

const apiCall = (url, method, params = null, data = null, customHeader = null) => {
  const user = JSON.parse(sessionStorage.getItem('USER'));
  let headers = {
    'Content-Type': 'application/json',
    Accept: 'application/json',
    ...customHeader,
  };
  if (user) {
    headers = {
      ...headers,
      Authorization: user.token,
    };
  }
  return axios({
    method,
    params,
    url: `${SITE_API}${url}`,
    data,
    headers,
  });
};

export default apiCall;
