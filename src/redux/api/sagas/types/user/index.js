import { takeLatest } from 'redux-saga/effects';

import { LOGIN } from 'redux/types';
import Sagas from 'redux/api/sagas/sagas';

// eslint-disable-next-line import/prefer-default-export
export function* login() {
  yield takeLatest(LOGIN, Sagas);
}
