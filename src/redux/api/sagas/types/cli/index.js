import { takeLatest } from 'redux-saga/effects';
import {
  CREATE, CREATE_INTEGRATION, DOWNLOAD, GET_GROUP, GET_LIST, GET_TEMPLATE,
} from 'redux/types';
import Sagas from 'redux/api/sagas/sagas';

export function* createProject() {
  yield takeLatest(CREATE, Sagas);
} export function* createProjectIntegration() {
  yield takeLatest(CREATE_INTEGRATION, Sagas);
}

export function* getListCli() {
  yield takeLatest(GET_LIST, Sagas);
}
export function* download() {
  yield takeLatest(DOWNLOAD, Sagas);
}
export function* getGroup() {
  yield takeLatest(GET_GROUP, Sagas);
}
export function* getTemplates() {
  yield takeLatest(GET_TEMPLATE, Sagas);
}
