import apiCall from 'redux/api/axios/api';
import { call, put } from 'redux-saga/effects';
import { LOGIN_SUCCESS } from '../../types';

function* Sagas(response) {
  const { payload } = response;
  const {
    url, method, success, error, params, data,
  } = payload;
  try {
    let pa = {};
    if (success !== LOGIN_SUCCESS) {
      const user = JSON.parse(sessionStorage.getItem('USER'));
      pa = {
        Authorization: user.token,
        'Content-Type': 'application/json',
      };
    }
    const result = yield call(apiCall, url, method, params || null, data || null, pa);
    // let send = result.data;
    // if (success === LOGIN_SUCCESS) send = { ...data, ...result.data };
    yield put({
      type: success,
      payload: result.data,
    });
  } catch (err) {
    yield put({
      type: error,
      payload: err,
    });
  }
}
export default Sagas;
