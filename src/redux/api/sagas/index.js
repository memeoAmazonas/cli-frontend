import { all } from 'redux-saga/effects';
import {
  createProject, createProjectIntegration, getListCli, download, getGroup,
  getTemplates,
} from './types/cli';
import { login } from './types/user';

export default function* rootSaga() {
  yield all([
    createProject(),
    createProjectIntegration(),
    download(),
    getGroup(),
    getListCli(),
    getTemplates(),
    login(),
  ]);
}
