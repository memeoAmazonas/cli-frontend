export const TYPE_API_TECHNICAL = [
  {
    name: 'api.type.technical.core',
    id: 'core',
  },
  {
    name: 'api.type.technical.product',
    id: 'producto',
  },

  {
    name: 'api.type.technical.entity',
    id: 'entidad',
  },
  {
    name: 'api.type.technical.discrete',
    id: 'discreta',
  }];

export const TYPE_LANGUAGE = ['Java'];

export const TYPE_JAVA_VERSION = {
  Java: ['1.8', '11', '16'],
};

export const TYPE_FRAMEWORK = {
  Java: ['Micronauta', 'Quarkus', 'springboot'],
};

export const TYPE_BD = ['Oracle', 'Mongo'];

export const TYPE_DEPENDENCY = ['MyBatis', 'Rx-Java', 'Apache-kafka', 'Org Mongo', 'JDBC'];
