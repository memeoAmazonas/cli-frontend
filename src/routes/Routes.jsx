import React from 'react';
import axios from 'axios';
import { Route, Switch, useHistory } from 'react-router-dom';

import SignIn from 'view/signIn/SignIn';
import Home from 'view/home/Home';
import { useDispatch } from 'react-redux';
import { ActionAsync } from 'redux/action';
import { LOADING, LOGOUT, SELECT_TYPE_API } from 'redux/types';
import { Toast } from 'component';

const data = [
  {
    component: <SignIn />,
    path: '/',
  },
  {
    component: <Home />,
    path: '/home',
  },
];
const Routes = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const [mes, setMes] = React.useState('empty');
  const [sev, setSev] = React.useState('success');
  const [open, setOpen] = React.useState(false);
  React.useEffect(() => {
    if (open) {
      const timeout = setTimeout(() => {
        setOpen(false);
      }, 4500);
      return () => clearTimeout(timeout);
    }
    return null;
  }, [open]);
  axios.interceptors.response.use(
    (response) => {
      dispatch(ActionAsync(LOADING, false));
      if (response.data && response.data.code === 400) {
        setOpen(true);
        setMes(response.data.key);
        setSev('error');
      }
      if (response.data && response.data.code === 201) {
        setSev('success');
        setOpen(true);
        setMes(response.data.key);
      }
      return response;
    },
    (error) => {
      dispatch(ActionAsync(LOADING, false));
      setOpen(true);
      if (error.response.status === 401) {
        setSev('warning');
        setMes('timeout');
        dispatch(ActionAsync(LOGOUT));
        history.push('/');
      }
      if (error.response.status === 400) {
        setSev('error');
        setMes(error.response.data.key);
        if (error.response.data.key === 'AUTH_ERR_GROUP')dispatch(ActionAsync(LOGOUT));
      }
      if (error.response.status === 500) {
        setSev('error');
        setMes('error.server');
        dispatch(ActionAsync(SELECT_TYPE_API));
        history.push('/home');
      }
      return Promise.reject(error);
    },
  );
  axios.interceptors.request.use(
    (request) => {
      dispatch(ActionAsync(LOADING, true));
      return request;
    },
    (error) => Promise.reject(error),
  );
  return (
    <Switch>
      {
                data.map((item) => (
                  <Route key={item.path} exact path={item.path}>
                    <>
                      {item.component}
                      <Toast severity={sev} open={open} message={mes} />
                    </>
                  </Route>
                ))
            }
    </Switch>
  );
};
export default Routes;
