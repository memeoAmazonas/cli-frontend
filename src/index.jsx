import React from 'react';
import ReactDom from 'react-dom';
import App from 'view/layout/App';

ReactDom.render(<App />, document.getElementById('root'));
