import makeStyles from '@material-ui/core/styles/makeStyles';

const useClass = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  form: {
    padding: 10,
    marginRight: 20,
    marginBottom: 20,
    maxWidth: 450,
    minWidth: 300,
    maxHeight: 450,
    display: 'flex',
    flexDirection: 'column',
  },
  formFeature: {
    padding: 10,
    marginRight: 20,
    marginBottom: 20,
  },
  feature: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  component: {
    display: 'flex',
    flexDirection: 'column',
  },
  title: {
    marginBottom: 20,
    width: '100%',
  },
  mrLeft: {
    marginLeft: 30,
  },
  mrLeftAndFlex: {
    marginLeft: 30,
    flexDirection: 'column',
    display: 'flex',
  },
  divider: {
    marginBottom: 20,
    backgroundColor: theme.palette.blue1,
    width: '100%',
  },
  detail: {
    maxWidth: 690,
    padding: 10,
    display: 'flex',
    flexWrap: 'wrap',
  },
  detailContentButton: {
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
  },
}));
export default useClass;
