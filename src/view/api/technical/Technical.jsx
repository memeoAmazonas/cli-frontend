import React from 'react';
import { keys, omit } from 'lodash';
import { ApiOptionContext } from 'context/api/Technical';
import {
  CustomRadioButton, Font, LinkButton,
} from 'component';
import { TYPE_JAVA_VERSION, TYPE_LANGUAGE } from 'assets/constants/types';
import { Box, Divider } from '@material-ui/core';
import GetRedux from 'redux/selector/getSelector';
import { KEY_GET_TEMPLATES, KEY_LOADING, KEY_TYPE_API } from 'redux/selector/keys';
import useClass from '../styles';
import InputText from '../type/InputText';
import SkeletonLoadig from '../Skeleton/SkeletonLoadig';
import { setVersion } from '../service';
import Details from '../detail/Details';

const Technical = () => {
  const classes = useClass();
  const [options, setOptions] = React.useContext(ApiOptionContext);
  const [info, setInfo] = React.useState({});
  const typeApi = GetRedux(KEY_TYPE_API);
  const loading = GetRedux(KEY_LOADING);
  const templates = GetRedux(KEY_GET_TEMPLATES);
  React.useEffect(() => {
    setOptions({ type: 'technical' });
  }, [typeApi]);

  const onChange = (e, key, remove = []) => {
    let val = e.target.value;
    if (key === 'version') {
      val = setVersion(e);
    }
    const typesInfo = omit(info, remove);
    setInfo({ ...typesInfo, [key]: val });
    const typeOptions = omit(options, remove);
    setOptions({ ...typeOptions, [key]: val });
  };
  const onClear = () => {
    setInfo({});
    setOptions({ type: 'technical' });
  };
  const regex = /^[A-Za-z0-9_]{4,}$/;
  const regexVersion = /^[0-9.]{1,}$/;
  if (!loading) {
    return (
      <div className={classes.root}>
        <div className={classes.title}>
          <Font id="config.api.tech" className="titleOption" />
          <Divider className={classes.divider} />
          {keys(info).length > 0 && <LinkButton onClick={onClear}><Font id="clear" /></LinkButton>}
        </div>
        <InputText
          title="name.core"
          tooltip="feature.tooltip"
          label="Core"
          regex={regex}
          onChange={(e) => onChange(e, 'core')}
          value={info.core || ''}
        />
        <InputText
          title="name.function"
          tooltip="feature.tooltip"
          label="name.function"
          regex={regex}
          onChange={(e) => onChange(e, 'product')}
          value={info.product || ''}
        />
        <InputText
          title="feature.name"
          tooltip="feature.tooltip"
          label="Feature"
          regex={regex}
          onChange={(e) => onChange(e, 'feature')}
          value={info.feature || ''}
        />
        <InputText
          title="version.number"
          tooltip="version.tooltip"
          label="Version"
          regex={regexVersion}
          onChange={(e) => onChange(e, 'version')}
          value={info.version || ''}
        />
        {info.core && regex.test(info.core) && (
        <Box boxShadow={3} className={classes.form}>
          <CustomRadioButton
            value={info.language || ''}
            data={TYPE_LANGUAGE}
            onChange={(w) => onChange(w, 'language', ['framework'])}
            title="select.language"
          />
          <>
            {info.language && (
            <CustomRadioButton
              title="language.version"
              valuesTitle={{ version: info.language }}
              value={info.version_language}
              data={TYPE_JAVA_VERSION[info.language]}
              onChange={(l) => onChange(l, 'version_language')}
            />
            )}
          </>
        </Box>
        )}
        {info.language && info.version_language && templates && (
        <Box boxShadow={3} className={classes.form}>
          <CustomRadioButton
            onChange={(w) => onChange(w, 'framework')}
            data={templates}
            keyValue="framework"
            keyLabel="framework"
            value={info.framework || ''}
            title="select.framework"
          />
        </Box>
        )}
        <Details onClear={onClear} />
      </div>
    );
  }
  if (loading) {
    return (<SkeletonLoadig />);
  }
  return null;
};

export default Technical;
