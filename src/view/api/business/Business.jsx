import Box from '@material-ui/core/Box';
import { Divider } from '@material-ui/core';
import { keys, omit, filter } from 'lodash';
import React from 'react';
import { useDispatch } from 'react-redux';

import {
  KEY_DATA_LIST, KEY_GET_GROUP, KEY_GET_TEMPLATES, KEY_LOADING, KEY_TYPE_API,
} from 'redux/selector/keys';
import { TYPE_JAVA_VERSION, TYPE_LANGUAGE } from 'assets/constants/types';
import { CustomRadioButton, Font, LinkButton } from 'component';
import { ApiOptionContext } from 'context/api/Technical';
import { ActionSagas } from 'redux/action';
import { GET_GROUP_TO_SEND, LIST_TO_SEND } from 'redux/api/axios/dataToSend';
import { GET_GROUP, GET_LIST } from 'redux/types';
import GetRedux from 'redux/selector/getSelector';

import Group from '../group/Group';
import useClass from '../styles';
import InputText from '../type/InputText';
import Details from '../detail/Details';
import SkeletonLoadig from '../Skeleton/SkeletonLoadig';
import { combineData, setVersion } from '../service';

const Business = () => {
  const classes = useClass();
  const data = GetRedux(KEY_DATA_LIST);
  const group = GetRedux(KEY_GET_GROUP);
  const [options, setOptions] = React.useContext(ApiOptionContext);
  const [domainDetail, setDomainDetail] = React.useState([]);
  const typeApi = GetRedux(KEY_TYPE_API);
  const dispatch = useDispatch();
  const loading = GetRedux(KEY_LOADING);
  const templates = GetRedux(KEY_GET_TEMPLATES);
  React.useEffect(() => {
    if (!group) {
      const { url } = GET_GROUP_TO_SEND;
      dispatch(ActionSagas(GET_GROUP, { ...GET_GROUP_TO_SEND, url: `${url}integration` }));
    }
  }, [typeApi]);
  React.useEffect(() => {
    if (!data) dispatch(ActionSagas(GET_LIST, LIST_TO_SEND));
  }, []);
  React.useEffect(() => {
    setOptions({
      type: 'integration',
      country: 'cl',
      scope: 'banking',
    });
  }, [typeApi]);

  const onClear = () => {
    setOptions({
      type: 'integration',
      country: 'cl',
      scope: 'banking',
    });
  };
  const onChange = (e, key, remove = []) => {
    let val = e.target.value;
    if (key === 'version') {
      val = setVersion(e);
    }
    if (key === 'framework') {
      const project = filter(templates, (ef) => ef.framework === val);
      console.log(project);
    }
    if (key === 'domain') {
      setDomainDetail(combineData(data[`${val}_LIST_KEYS`], data[`${val}_LIST_NAMES`], false));
    }
    const inf = omit(options, remove);
    setOptions({ ...inf, [key]: val });
  };
  const stylesOverflow = domainDetail && domainDetail.length > 10 ? {
    overflow: 'scroll',
    overflowX: 'hidden',
  } : {};
  const remove = ['capacity', 'language', 'version_language', 'version', 'framework', 'feature'];
  const regex = /^[A-Za-z0-9_]{4,}$/;
  const regexVersion = /^[0-9.]{1,}$/;

  const validatePoint = () => {
    const { version } = options;
    if (version.charAt(version.length - 1) === '.') {
      setOptions({ ...options, version: version.slice(0, -1) });
    }
    return true;
  };

  if (group && data && !loading) {
    return (
      <div className={classes.root}>
        <div className={classes.title}>
          <Font id="config.api.business" className="titleOption" />
          <Divider className={classes.divider} />
          {keys(options).length > 3 && <LinkButton onClick={onClear}><Font id="clear" /></LinkButton>}
        </div>
        <Box boxShadow={3} className={classes.form}>
          <Group onChange={(w) => onChange(w, 'group')} value={options.group || ''} />
        </Box>
        {data && (
        <Box boxShadow={3} className={classes.form}>
          <CustomRadioButton
            value={options.domain || ''}
            onChange={(w) => onChange(w, 'domain', remove)}
            data={combineData(data.DOMAIN_LIST_KEYS, data.DOMAIN_LIST_NAMES)}
            title="select.domain"
            keyLabel="name"
            keyValue="id"
          />
        </Box>
        )}
        {options && options.domain && (
        <Box
          boxShadow={3}
          className={classes.form}
          style={stylesOverflow}
        >
          <CustomRadioButton
            value={options.capacity || ''}
            onChange={(w) => onChange(w, 'capacity')}
            data={domainDetail}
            title="select.domain.capacity"
            keyLabel="name"
            keyValue="id"
            valuesTitle={{ domain: options.domain }}
          />
        </Box>
        )}
        <Box boxShadow={3} className={classes.form}>
          <CustomRadioButton
            value={options.language || ''}
            data={TYPE_LANGUAGE}
            onChange={(w) => onChange(w, 'language', ['framework'])}
            title="select.language"
          />
          <>
            {options.language && (
            <CustomRadioButton
              title="language.version"
              valuesTitle={{ version: options.language }}
              value={options.version_language}
              data={TYPE_JAVA_VERSION[options.language]}
              onChange={(l) => onChange(l, 'version_language')}

            />
            )}
          </>
        </Box>
        {options.language && templates && (
        <Box boxShadow={3} className={classes.form}>
          <CustomRadioButton
            onChange={(w) => onChange(w, 'framework')}
            data={templates}
            keyValue="framework"
            keyLabel="framework"
            value={options.framework || ''}
            title="select.framework"
          />
        </Box>
        )}
        {options.framework && (
        <InputText
          title="feature.name"
          tooltip="feature.tooltip"
          label="Feature"
          regex={regex}
          onChange={(e) => onChange(e, 'feature')}
          value={options.feature}
        />
        )}
        {options.feature && regex.test(options.feature) && (
        <InputText
          title="version.number"
          tooltip="version.tooltip"
          label="Version"
          regex={regexVersion}
          onChange={(e) => onChange(e, 'version')}
          validate={validatePoint}
          value={options.version || ''}
        />
        )}
        <Details onClear={onClear} />
      </div>
    );
  }
  if (loading || !group || !data) {
    return (<SkeletonLoadig />);
  }
  return null;
};
export default Business;
