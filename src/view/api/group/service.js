import apiCall from 'redux/api/axios/api';
import { GROUPS_URL } from 'redux/api/axios/url';

// eslint-disable-next-line import/prefer-default-export
export const GetGroups = async (type) => {
  try {
    return await apiCall(GROUPS_URL + type, 'GET');
  } catch (e) {
    return 'error';
  }
};
