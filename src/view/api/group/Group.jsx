import React from 'react';
import GetRedux from 'redux/selector/getSelector';
import { KEY_GET_GROUP, KEY_TYPE_API } from 'redux/selector/keys';
import { CustomRadioButton } from 'component';
import { useDispatch } from 'react-redux';
import { ActionSagas } from 'redux/action';
import { GET_GROUP, GET_LIST } from 'redux/types';
import { GET_GROUP_TO_SEND, LIST_TO_SEND } from 'redux/api/axios/dataToSend';

const Group = (props) => {
  const typeApi = GetRedux(KEY_TYPE_API);
  const group = GetRedux(KEY_GET_GROUP);
  const dispatch = useDispatch();
  React.useEffect(() => {
    if (!group) {
      let type = 'integration';
      if (typeApi === 1) {
        type = 'technical';
      }
      const { url } = GET_GROUP_TO_SEND;
      dispatch(ActionSagas(GET_GROUP, { ...GET_GROUP_TO_SEND, url: url + type }));
      dispatch(ActionSagas(GET_LIST, LIST_TO_SEND));
    }
  }, [typeApi]);
  if (group) {
    return (
      <CustomRadioButton data={group} keyLabel="name" keyValue="name" title="group.gitlab" {...props} />
    );
  }
  return null;
};

export default Group;
