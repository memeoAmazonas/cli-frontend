export const combineData = (first, second, isKey = true) => first.map((i, k) => ({
  name: `${second[k]} (${i})`,
  id: isKey ? i : second[k],
}));

export const setVersion = (e) => {
  let val = e.target.value;
  val = val.replace(/[^0-9.]/g, '');
  if (val.charAt(val.length - 1) === '.' && val.charAt(val.length - 2) === '.') {
    val = val.slice(0, -1);
  }
  if (val.charAt(0) === '.') {
    val = val.slice(1);
  }
  val = val.replace('..', '.');
  return val;
};
