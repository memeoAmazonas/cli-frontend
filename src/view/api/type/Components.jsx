import React from 'react';
import Box from '@material-ui/core/Box';
import { CustomCheckBox, Font } from 'component';
import { Divider } from '@material-ui/core';
import useClass from '../styles';

const Components = ({
  onSelectComponent, comp,
}) => {
  const classes = useClass();

  return (
    <Box boxShadow={3} className={classes.form}>
      <Font id="select.component" className="titleOptionApi" />
      <div className={classes.component}>
        <CustomCheckBox checked={comp.bd || false} name="bd" onChange={onSelectComponent} label="bd" />
        {comp.bd && (
        <>
          <Divider />
          <div className={classes.mrLeftAndFlex}>
            <Font id="select.bd" className="titleOptionApi" />
            <CustomCheckBox
              checked={comp.oracle || false}
              name="oracle"
              onChange={onSelectComponent}
              label="Oracle"
            />
            <CustomCheckBox
              checked={comp.mongo || false}
              name="mongo"
              onChange={onSelectComponent}
              label="Mongo"
            />
          </div>
          <Divider />
        </>
        )}
        <CustomCheckBox
          checked={comp.kafka || false}
          name="kafka"
          onChange={onSelectComponent}
          label="kafka"
        />
        <CustomCheckBox
          checked={comp.http || false}
          name="http"
          onChange={onSelectComponent}
          label="http"
        />
      </div>
    </Box>
  );
};

export default Components;
