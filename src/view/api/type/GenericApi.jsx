import React from 'react';
import { filter, keys, omit } from 'lodash';
import { ApiOptionContext } from 'context/api/Technical';
import {
  CustomCheckBox,
  CustomRadioButton, Font, LinkButton,
} from 'component';
import {
  TYPE_API_TECHNICAL, TYPE_BD,
  TYPE_DEPENDENCY, TYPE_FRAMEWORK, TYPE_JAVA_VERSION, TYPE_LANGUAGE,
} from 'assets/constants/types';
import { Box, Divider } from '@material-ui/core';
import useClass from '../styles';
import InputText from './InputText';

const GenericApi = ({
  title, detail, info, setInfo, head,
}) => {
  const classes = useClass();
  const [options, setOptions] = React.useContext(ApiOptionContext);
  const [comp, setComp] = React.useState({});
  const [dep, setDep] = React.useState({});

  React.useEffect(() => {
    setOptions({});
  }, []);
  const onChange = (e, key, remove = []) => {
    if (key === 'typeApiTech') {
      setComp({});
      setDep({});
    }
    const typesInfo = omit(info, remove);
    setInfo({ ...typesInfo, [key]: e.target.value });
    const typeOptions = omit(options, remove);
    setOptions({ ...typeOptions, [key]: e.target.value });
  };
  const onSelectComponent = (e) => {
    if (!e.target.checked) {
      const component = filter(options.component, (el) => el !== e.target.name);
      setOptions({ ...options, component });
    } else {
      const component = options.component || [];
      component.push(e.target.name);
      setOptions({ ...options, component });
    }
    setComp({ ...comp, [e.target.name]: e.target.checked });
  };
  const onSelectDependency = (e) => {
    if (!e.target.checked) {
      const dependencies = filter(options.dependencies, (el) => el !== e.target.name);
      setOptions({ ...options, dependencies });
    } else {
      const dependencies = options.dependencies || [];
      dependencies.push(e.target.name);
      setOptions({ ...options, dependencies });
    }
    setDep({ ...dep, [e.target.name]: e.target.checked });
  };
  const onClear = () => {
    setInfo({});
    setDep({});
    setComp({});
  };
  const regex = /^[A-Za-z0-9_]{4,}$/;
  const regexVersion = /^[A-Za-z0-9_.-]{1,}$/;
    const remove = ['domainDetail', 'language', 'versionLanguage', 'version', 'framework', 'component', 'typeBd', 'feature', 'dependencies'];
  return (
    <div className={classes.root}>
      <div className={classes.title}>
        <Font id={title} className="titleOption" />
        <Divider className={classes.divider} />
        {keys(info).length > 0 && <LinkButton onClick={onClear}><Font id="clear" /></LinkButton>}
      </div>
      <Box boxShadow={3} className={classes.form}>
        <CustomRadioButton
          value={info.typeApiTech || ''}
          onChange={(w) => onChange(w, 'typeApiTech',
            remove)}
          data={TYPE_API_TECHNICAL}
          keyLabel="name"
          keyValue="id"
          title="type.technical"
        />
      </Box>

      {info.typeApiTech && (
        <Box boxShadow={3} className={classes.form}>
          <CustomRadioButton
            value={info.language || ''}
            data={TYPE_LANGUAGE}
            onChange={(w) => onChange(w, 'language', ['framework'])}
            title="select.language"
          />
          <>
            {info.language && (
            <CustomRadioButton
              title="language.version"
              valuesTitle={{ version: info.language }}
              value={info.versionLanguage}
              data={TYPE_JAVA_VERSION[info.language]}
              onChange={(l) => onChange(l, 'versionLanguage')}
            />
            )}
          </>
        </Box>
      )}
      {info.language && info.versionLanguage && (
        <Box boxShadow={3} className={classes.form}>
          <CustomRadioButton
            onChange={(w) => onChange(w, 'framework')}
            data={TYPE_FRAMEWORK[info.language]}
            value={info.framework || ''}
            title="select.framework"
          />
        </Box>
      )}
      {info.framework && (
        <InputText
          title="feature.name"
          tooltip="feature.tooltip"
          label="Feature"
          regex={regex}
          onChange={(e) => onChange(e, 'feature')}
          value={info.feature}
        />
      )}
      {info.feature && regex.test(info.feature) && (
        <InputText
          title="version.number"
          tooltip="version.tooltip"
          label="Version"
          regex={regexVersion}
          onChange={(e) => onChange(e, 'version')}
          value={info.version}
        />
      )}
      {info.feature && regex.test(info.feature)
            && info.version && regexVersion.test(info.version) && (
            <Box boxShadow={3} className={classes.form}>
              <Font id="select.component" className="titleOptionApi" />
              <div className={classes.component}>
                <CustomCheckBox checked={comp.bd || false} name="bd" onChange={onSelectComponent} label="bd" />
                {comp.bd && (
                <>
                  <Divider />
                  <CustomRadioButton
                    className={classes.mrLeft}
                    value={info.typeBd || ''}
                    data={TYPE_BD}
                    onChange={(w) => onChange(w, 'typeBd')}
                    title="select.bd"
                  />
                  <Divider />
                </>
                )}
                <CustomCheckBox
                  checked={comp.kafka || false}
                  name="kafka"
                  onChange={onSelectComponent}
                  label="kafka"
                />
                <CustomCheckBox
                  checked={comp.http || false}
                  name="http"
                  onChange={onSelectComponent}
                  label="http"
                />
              </div>
            </Box>
      )}
      {info.feature && regex.test(info.feature) && info.version && regexVersion.test(info.version)
            && options.component && options.component.length > 0 && (
            <Box boxShadow={3} className={classes.form}>
              <Font id="select.dependency" className="titleOptionApi" />
              <div className={classes.component}>
                {
                            TYPE_DEPENDENCY.map((it) => (
                              <CustomCheckBox
                                key={it}
                                checked={dep[it] || false}
                                name={it}
                                onChange={onSelectDependency}
                                label={it}
                              />
                            ))
                        }

              </div>
            </Box>
      )}
        {detail}
    </div>
  );
};

export default GenericApi;
