import React from 'react';
import { Box, Fade, Tooltip } from '@material-ui/core';
import HelpIcon from '@material-ui/icons/Help';
import { Font, Input } from 'component';
import theme from 'theme';
import useClass from 'view/api/styles';

const InputText = ({
  title, tooltip, label, regex, onChange, value, validate = () => true,
  width = 300, complement = null,
}) => {
  const classes = useClass();
  const [errFeature, setErrFeature] = React.useState(false);
  const err = value && errFeature;
  return (
    <Box boxShadow={3} className={classes.formFeature} style={{ width }}>
      <div className={classes.feature}>
        <Font id={title} className="titleOptionApi" />
        <Tooltip
          placement="top"
          TransitionComponent={Fade}
          TransitionProps={{ timeout: 600 }}
          title={<Font id={tooltip} />}
        >
          <HelpIcon htmlColor={theme.palette.blue1} />
        </Tooltip>
      </div>
      <Input
        error={err}
        fullWidth
        label={label}
        margin="normal"
        variant="outlined"
        required
        value={value}
        onChange={onChange}
        onBlur={() => setErrFeature(!(regex.test(value) && validate()))}
        onFocus={() => setErrFeature(false)}
        helperText={value && errFeature
                && <Font id={tooltip} rest={{ color: 'error', variant: 'body1', component: 'span' }} />}
      />
      {complement}
    </Box>
  );
};

export default InputText;
