import React from 'react';

import { ApiContextProvider } from 'context/api/Technical';
import GetRedux from 'redux/selector/getSelector';
import { KEY_TYPE_API } from 'redux/selector/keys';
import { CustomCard } from 'component';
import Technical from './technical/Technical';
import Business from './business/Business';

const Api = () => {
  const type = GetRedux(KEY_TYPE_API);
  if (type === 0) {
    return (
      <ApiContextProvider>
        <CustomCard margint="20px" padding="20px" width="100%" marginl="auto" marginr="auto">
          <Business />
        </CustomCard>
      </ApiContextProvider>
    );
  }
  if (type === 1) {
    return (
      <ApiContextProvider>
        <CustomCard margint="20px" padding="20px" width="100%" marginl="auto" marginr="auto">
          <Technical />
        </CustomCard>
      </ApiContextProvider>
    );
  }
  return null;
};

export default Api;
