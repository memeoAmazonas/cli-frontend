import React from 'react';
import { Divider } from '@material-ui/core';
import Box from '@material-ui/core/Box';
import { uniqueId } from 'lodash';
import Skeleton from '@material-ui/lab/Skeleton';
import useClass from '../styles';

const SkeletonLoadig = () => {
  const classes = useClass();
  return (
    <div className={classes.root}>
      <div className={classes.title}>
        <Divider className={classes.divider} />
      </div>
      {Array.from(Array(5).keys()).map(() => (
        <Box key={uniqueId()} boxShadow={3} className={classes.form}>
          <Skeleton height={65} width="100%" />
          <Skeleton height={65} width="80%" />
          <Skeleton height={65} width="80%" />
          <Skeleton height={65} width="80%" />
        </Box>
      ))}
    </div>
  );
};

export default SkeletonLoadig;
