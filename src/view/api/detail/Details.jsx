import React from 'react';
import { filter, omit } from 'lodash';
import Box from '@material-ui/core/Box';
import Divider from '@material-ui/core/Divider';
import { useDispatch } from 'react-redux';

import { ApiOptionContext } from 'context/api/Technical';
import GetRedux from 'redux/selector/getSelector';
import {
  KEY_TYPE_API, KEY_DATA_LIST, KEY_GET_GROUP, KEY_CREATE_SUCCESS,
  KEY_CREATE_INTEGRATION_SUCCESS, KEY_GET_TEMPLATES,
} from 'redux/selector/keys';
import { Font, LabelDetail, PrincipalBtn } from 'component';
import {
  CREATE, CREATE_INTEGRATION, CREATE_INTEGRATION_SUCCESS, CREATE_SUCCESS, SELECT_TYPE_API,
} from 'redux/types';
import { CREATE_PROJECT, CREATE_PROJECT_INTEGRATION } from 'redux/api/axios/dataToSend';
import { ActionSagas, ActionAsync } from 'redux/action';
import GetDownloadTech, { getGroup, setDomain, validate } from './service';
import useClass from '../styles';

const Details = ({ onClear }) => {
  const [options, setOptions] = React.useContext(ApiOptionContext);
  const dispatch = useDispatch();
  const typeApi = GetRedux(KEY_TYPE_API);
  const lists = GetRedux(KEY_DATA_LIST);
  const groupx = GetRedux(KEY_GET_GROUP);
  const templates = GetRedux(KEY_GET_TEMPLATES);
  const codeCreate = GetRedux(KEY_CREATE_SUCCESS);
  const codeCreateIntegration = GetRedux(KEY_CREATE_INTEGRATION_SUCCESS);
  const user = JSON.parse(sessionStorage.getItem('USER'));
  React.useEffect(() => {
    if (codeCreate && codeCreate.code === 201 && user && user.token) {
      GetDownloadTech('project', user.token);
      dispatch(ActionAsync(CREATE_SUCCESS));
      dispatch(ActionAsync(SELECT_TYPE_API));
    }
  }, [codeCreate, user]);
  React.useEffect(() => {
    if (codeCreateIntegration && codeCreateIntegration.code === 201 && user && user.token) {
      GetDownloadTech(options.feature, user.token);
      dispatch(ActionAsync(CREATE_INTEGRATION_SUCCESS));
      dispatch(ActionAsync(SELECT_TYPE_API));
    }
  }, [codeCreate, user]);
  const classes = useClass();
  const onSend = () => {
    const id = filter(templates, (w) => w.framework === options.framework);
    let data = { ...options, compilation: 'graddle', id_project: id[0].project };
    let action = CREATE;
    let pay = CREATE_PROJECT;
    if (options.type === 'integration') {
      data = omit(data, ['compilation']);
      action = CREATE_INTEGRATION;
      pay = CREATE_PROJECT_INTEGRATION;
      let { group, domain, capacity } = options;
      capacity = capacity.replaceAll('-', '_');
      domain = setDomain(lists, domain);
      group = getGroup(groupx, group);
      data = {
        ...data, capacity, domain, group,
      };
    }
    if (options.type === 'technical') {
      data.core = `ms_${data.core.replaceAll('-', '_')}`;
    }
    const payload = { data: omit(data, ['type']), ...pay };
    dispatch(ActionSagas(action, payload));
  };
  const onClean = () => {
    setOptions({});
    onClear();
  };
  if (validate(options)) {
    return (
      <div className={classes.title}>
        <Divider className={classes.divider} />
        <Font
          id={typeApi !== 0 ? 'config.api.tech.detail'
            : 'config.api.business.detail'}
          className="titleOption"
        />
        <Box boxShadow={3} className={classes.detail}>
          {options && options.core
          && <LabelDetail label="name.core" detail={options.core} />}
          {options && options.product
          && <LabelDetail label="name.function" detail={options.product} />}
          {options && options.group
          && <LabelDetail label="repository.group" detail={options.group} />}
          {options && options.domain
          && <LabelDetail label="domain" detail={options.domain} />}
          {options && options.capcity
          && <LabelDetail label="capacity" detail={options.capacity} />}
          {options && options.version
          && <LabelDetail label="version.number" detail={options.version} /> }
          {options && options.language
          && <LabelDetail label="language" detail={options.language} /> }
          {options && options.version_language && (
          <LabelDetail
            label="language.version"
            valuesLabel={{ version: options.language }}
            detail={options.version_language}
          />
          )}
          { options && options.framework
          && <LabelDetail label="framework" detail={options.framework} /> }
          {options && options.feature
          && <LabelDetail label="feature.name" detail={options.feature} /> }
          <Divider className={classes.divider} />
          <div className={classes.detailContentButton}>
            <PrincipalBtn mr="20px" onClick={onSend}><Font id="create" /></PrincipalBtn>
            <PrincipalBtn bg="red" onClick={onClean}><Font id="clear" /></PrincipalBtn>
          </div>
        </Box>
      </div>
    );
  }
  return null;
};

export default Details;
