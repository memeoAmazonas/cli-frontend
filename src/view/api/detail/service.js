import axios from 'axios';
import { indexOf, keys } from 'lodash';

import { DOWNLOAD_TECH } from 'redux/api/axios/url';

const SITE_API = process.env.REACT_APP_API_BASE;
const GetDownloadTech = (name = 'project', Authorization = '') => {
  axios.request({
    url: `${SITE_API}${DOWNLOAD_TECH}`,
    method: 'get',
    headers: { Authorization },
    responseType: 'blob',
  })
    .then(({ data }) => {
      const downloadUrl = window.URL.createObjectURL(new Blob([data]));
      const link = document.createElement('a');
      link.href = downloadUrl;
      link.setAttribute('download', `${name}.zip`);
      document.body.appendChild(link);
      link.click();
      link.remove();
    });
};
export default GetDownloadTech;

export const validate = (op) => {
  const regex = /^[A-Za-z0-9_]{4,}$/;
  const regexVersion = /^[0-9.]{1,}$/;
  if (op && op.type === 'integration') {
    const k = ['domain', 'capacity', 'language', 'version_language', 'framework', 'feature', 'version'];
    return k.every((i) => keys(op).includes(i))
            && regex.test(op.feature)
            && regexVersion.test(op.version);
  }
  const k = ['core', 'product', 'language', 'version_language', 'framework', 'feature', 'version'];
  return k.every((i) => keys(op).includes(i))
        && regex.test(op.feature)
        && regexVersion.test(op.version)
        && regex.test(op.core)
        && regex.test(op.product);
};

export const setDomain = (data, value) => {
  const index = indexOf(data.DOMAIN_LIST_KEYS, value, 0);
  return data.DOMAIN_LIST_NAMES[index].replaceAll('-', '_');
};
export const getGroup = (data, key) => data.find((el) => el.name === key).id;
