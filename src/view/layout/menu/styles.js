import makeStyles from '@material-ui/core/styles/makeStyles';

const menuClasses = makeStyles((th) => ({
  contentMenu: {
    alignItems: 'center',
    display: 'flex',
  },
  list: {
    backgroundColor: th.palette.grey3,
    height: '100%',
    width: 350,
  },
  fullList: {
    width: 'auto',
  },
  contentList: {
    color: th.palette.common.white,
    paddingTop: 64,
  },
  divider: {
    backgroundColor: th.palette.common.white,
    opacity: 0.5,
  },
  image: {
    height: 130,
    marginBottom: 20,
    marginLeft: '25%',
    width: 130,
  },
  itemList: {
    paddingLeft: 20,
    '&:hover': {
      backgroundColor: th.palette.blue1,
    },
  },
  name: {
    display: 'flex',
    fontSize: 20,
    justifyItems: 'flex-end',
  },
}));

export default menuClasses;
