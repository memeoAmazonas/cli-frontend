import React from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { Menu as IconMenu } from '@material-ui/icons';

import {
  Divider, Drawer, IconButton, List, ListItem, ListItemIcon, ListItemText,
} from '@material-ui/core';
import PowerSettingsNewIcon from '@material-ui/icons/PowerSettingsNew';
import theme from 'theme';
import GetRedux from 'redux/selector/getSelector';
import { KEY_SET_TITLE_PAGE } from 'redux/selector/keys';
import { Font } from 'component';
import Items from 'view/layout/menu/Items';
import ExpireSession from 'component/utilities/ExpireSession';
import { ActionAsync } from 'redux/action';
import { LOGOUT } from 'redux/types';
import WhiteIcon from 'component/utilities/WhiteIcon';
import menuClasses from './styles';

const Menu = () => {
  const classes = menuClasses();
  const isLogged = sessionStorage.getItem('USER');
  const history = useHistory();
  const dispatch = useDispatch();
  const title = GetRedux(KEY_SET_TITLE_PAGE);
  const [openMenu, setOpenMenu] = React.useState(false);
  const closeMenu = () => {
    setOpenMenu(false);
  };
  const close = async () => {
    closeMenu();
    dispatch(ActionAsync(LOGOUT));
    history.push('/');
  };
  const image = (
    <div className={classes.contentMenu}>
      <Font
        id={title || 'empty'}
        className={title !== 'name.app' ? 'titleLogged' : 'titleNotLogged'}
      />
    </div>
  );
  return (
    <div className={classes.contentMenu}>
      {isLogged ? (
        <>
          <ExpireSession />
          <IconButton onClick={() => setOpenMenu(true)}>
            <IconMenu htmlColor={theme.palette.blue1} fontSize="large" />
          </IconButton>
          {image}
        </>
      )
        : image}
      <Drawer anchor="left" open={openMenu} onClose={closeMenu}>
        <div className={classes.list}>
          <List className={classes.contentList}>
            <Divider className={classes.divider} />
            <Items closeMenu={closeMenu} />
            <ListItem
              button
              className={classes.itemList}
              onClick={close}
            >
              <ListItemIcon>
                {WhiteIcon(PowerSettingsNewIcon)}
              </ListItemIcon>
              <ListItemText primary={<Font id="logout" />} />
            </ListItem>
          </List>
        </div>
      </Drawer>
    </div>
  );
};

export default Menu;
