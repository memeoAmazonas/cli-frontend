import React from 'react';
import { Home } from '@material-ui/icons';
import {
  ListItem, ListItemText, ListItemIcon, Divider,
} from '@material-ui/core';
import { useHistory } from 'react-router-dom';

import menuClasses from 'view/layout/menu/styles';
import { Font } from 'component';
import WhiteIcon from 'component/utilities/WhiteIcon';

const items = [
  {
    label: 'home',
    icon: Home,
    to: '/home',
  },
];

const Items = ({ closeMenu }) => {
  const history = useHistory();
  const classes = menuClasses();
  const onRedirect = (to) => {
    closeMenu();
    history.push(to);
  };
  return items.map((item) => (
    <div key={item.to}>
      <ListItem
        button
        className={classes.itemList}
        onClick={() => onRedirect(item.to)}
      >
        <ListItemIcon>
          {WhiteIcon(item.icon)}
        </ListItemIcon>
        <ListItemText primary={<Font id={item.label} />} />
      </ListItem>
      <Divider className={classes.divider} />
    </div>
  ));
};

export default Items;
