import React from 'react';
import i18n from 'i18next';
import { useDispatch } from 'react-redux';
import {
  IconButton, Fade, Menu, MenuItem,
} from '@material-ui/core';
import LanguageIcon from '@material-ui/icons/Language';
import makeStyles from '@material-ui/core/styles/makeStyles';

import theme from 'theme';
import { Font } from 'component';
import { ActionAsync } from 'redux/action';
import { SET_LANGUAGE } from 'redux/types';
import { KEY_SET_LANGUAGE } from 'redux/selector/keys';
import GetRedux from 'redux/selector/getSelector';

const menuStyles = makeStyles((th) => ({
  root: {
    marginLeft: 15,
    marginTop: 28,
  },
  btnLang: {
    '&:hover': {
      background: 'none',
    },
  },
  btnOptionLang: {
    '&:hover': {
      background: th.palette.blue1,
      color: th.palette.common.white,
    },
  },
}));
const Language = () => {
  const classes = menuStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const language = GetRedux(KEY_SET_LANGUAGE);
  const dispatch = useDispatch();
  React.useEffect(() => {
    const localLang = localStorage.getItem('LANG');
    if (localLang && !language) {
      dispatch(ActionAsync(SET_LANGUAGE, localLang));
      i18n.changeLanguage(localLang);
    }
  });

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleLanguage = (lang) => {
    dispatch(ActionAsync(SET_LANGUAGE, lang));
    localStorage.setItem('LANG', lang);
    i18n.changeLanguage(lang);
    setAnchorEl(null);
  };
  return (
    <>
      <IconButton className={classes.btnLang} aria-controls="fade-menu" aria-haspopup="true" onClick={handleClick}>
        <>
          <LanguageIcon htmlColor={theme.palette.blue1} />
          <Font id={language || 'es'} className="languageField" />
        </>
      </IconButton>
      <Menu
        id="fade-menu"
        anchorEl={anchorEl}
        keepMounted
        open={open}
        onClose={() => setAnchorEl(null)}
        TransitionComponent={Fade}
        className={classes.root}
      >
        <MenuItem className={classes.btnOptionLang} onClick={() => handleLanguage('es')}><Font id="spanish" /></MenuItem>
        <MenuItem className={classes.btnOptionLang} onClick={() => handleLanguage('en')}><Font id="english" /></MenuItem>
      </Menu>
    </>
  );
};
export default Language;
