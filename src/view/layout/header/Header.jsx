import React from 'react';
import styled from 'styled-components';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import makeStyles from '@material-ui/core/styles/makeStyles';
import Container from '@material-ui/core/Container';
import theme from 'theme';
import Menu from 'view/layout/menu/Menu';
import Language from './Language';

const CustomHeader = styled(AppBar)`
&.MuiAppBar-root {
box-shadow: none;
background-color: ${theme.palette.common.white};
}`;

const CustomToolbar = styled(Toolbar)`
&.MuiToolbar-root{
box-shadow: 0px 2px 4px -1px rgb(0 0 0 / 20%), 0px 4px 5px 0px rgb(0 0 0 / 14%), 0px 1px 10px 0px rgb(0 0 0 / 12%);
}
`;

const useStyles = makeStyles((th) => ({
  content: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: th.spacing(2),
  },
}));
const Header = () => {
  const clasess = useStyles();
  return (
    <div className={clasess.root}>
      <CustomHeader position="static">
        <CustomToolbar>
          <Container maxWidth="xl" className={clasess.content}>
            <Menu />
            <Language />
          </Container>
        </CustomToolbar>
      </CustomHeader>
    </div>
  );
};

export default Header;
