import { createBrowserHistory } from 'history';
import React from 'react';
import { withTranslation } from 'react-i18next';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import styled from 'styled-components';
import { Container, Grid, ThemeProvider } from '@material-ui/core';

import 'intl';
import theme from 'theme';
import configureStore from 'redux/store/store';
import Routes from 'routes/Routes';
import Header from './header/Header';

const history = createBrowserHistory();
const store = configureStore();

const RootContainer = styled(Container)`
  &.MuiContainer-root {
  min-height: calc( 100vh - 64px);
  padding-right: ${(props) => (props.padding ? props.padding : '0')};
  padding-left: ${(props) => (props.padding ? props.padding : '0')};
  background-color: ${theme.palette.grey1};
  }
`;
const App = () => (
  <ThemeProvider theme={theme}>
    <Provider store={store}>
      <Router history={history}>
        <RootContainer maxWidth={false}>
          <Header />
          <RootContainer maxWidth="xl" padding="25px">
            <Grid container direction="column">
              <Routes />
            </Grid>
          </RootContainer>
        </RootContainer>
      </Router>
    </Provider>
  </ThemeProvider>
);

export default withTranslation()(App);
