import React from 'react';
// import { useHistory } from 'react-router-dom';

import { useDispatch } from 'react-redux';
import { ActionAsync } from 'redux/action';
import {
  GET_GROUP_SUCCESS,
  GET_TEMPLATE_SUCCESS, LOGIN_SUCCESS, SELECT_TYPE_API, SET_TITLE_PAGE,
} from 'redux/types';
import {
  ViewContainer, Font, PrincipalBtn, CustomCard, Toast,
} from 'component';
import Api from 'view/api/Api';
import GetRedux from 'redux/selector/getSelector';
import { KEY_DATA_USER, KEY_TYPE_API } from 'redux/selector/keys';
import Skeleton from '@material-ui/lab/Skeleton';
import apiCall from '../../redux/api/axios/api';

const Home = () => {
  // const history = useHistory();
  const dispatch = useDispatch();
  const typeApi = GetRedux(KEY_TYPE_API);
  const user = GetRedux(KEY_DATA_USER);
  React.useEffect(() => {
    dispatch(ActionAsync(SET_TITLE_PAGE, 'home'));
  }, []);
  React.useEffect(() => {
    const actUser = JSON.parse(sessionStorage.getItem('USER'));
    if (!user && actUser) {
      dispatch(ActionAsync(LOGIN_SUCCESS, actUser));
    }
  });
  const [visible, setVisible] = React.useState(false);
  const [open, setOpen] = React.useState(false);
  const [existTe, setExistTe] = React.useState({ x: false, y: false });
  const onHandle = async (type) => {
    setVisible(true);
    let api = 'technical';
    if (type === 0) {
      dispatch(ActionAsync(GET_GROUP_SUCCESS));
      api = 'integration';
    }
    const data = await apiCall(`cli/templates/list/${api}`, 'GET');
    dispatch(ActionAsync(GET_TEMPLATE_SUCCESS));
    if (data && data.data && data.data.length > 0) {
      dispatch(ActionAsync(GET_TEMPLATE_SUCCESS, data.data));
      dispatch(ActionAsync(SELECT_TYPE_API, type));
    } else {
      setOpen(true);
      type === 0
        ? setExistTe({ ...existTe, x: true })
        : setExistTe({ ...existTe, y: true });
    }

    setVisible(false);
  };
  React.useEffect(() => {
    if (open) {
      const timeout = setTimeout(() => {
        setOpen(false);
      }, 4500);
      return () => clearTimeout(timeout);
    }
    return null;
  }, [open]);
  if (user && user.email !== '') {
    return (
      <ViewContainer>
        <CustomCard margint="20px" padding="20px" width="25%">
          <Toast message="template.empty" open={open} severity="warning" />
          <Font id="select.type.api" className="titleOption" />
          {!visible && (
          <ViewContainer jc="flex-init" style={{ paddingLeft: 0 }}>

            <PrincipalBtn mb="20px" mr="20px" disabled={typeApi === 0 || existTe.x} onClick={() => onHandle(0)}>
              <Font
                id="business.integration"
              />
            </PrincipalBtn>
            <PrincipalBtn mb="20px" disabled={typeApi === 1 || existTe.y} onClick={() => onHandle(1)}>
              <Font
                id="technical"
              />
            </PrincipalBtn>
          </ViewContainer>
          )}
          {visible && (
          <ViewContainer>
            <Skeleton height={65} width="55%" />
            <Skeleton height={65} width="40%" />
          </ViewContainer>
          )}
        </CustomCard>
        <Api />
      </ViewContainer>

    );
  }
  return (
    <ViewContainer>
      <CustomCard margint="20px" padding="20px" width="25%">
        <h2>is admin</h2>
      </CustomCard>
    </ViewContainer>
  );
};
export default Home;
