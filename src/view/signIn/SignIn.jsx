import React from 'react';
import {
  ContentAlt, CustomCard, CustomContainer, Font, Input, LinkButton, PrincipalBtn, Toast,
} from 'component';
import imgLogin from 'assets/images/logo.png';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { LIST_TO_SEND, LOGIN_TO_SEND } from 'redux/api/axios/dataToSend';
import {
  GET_LIST, LOGIN, LOGIN_SUCCESS,
} from 'redux/types';
import { ActionAsync, ActionSagas } from 'redux/action';
import Skeleton from '@material-ui/lab/Skeleton';
import GetRedux from 'redux/selector/getSelector';
import { KEY_DATA_LIST, KEY_DATA_USER, KEY_LOADING_USER } from 'redux/selector/keys';
import SigninAdmin from './SigninAdmin';
import cl from './syles';

const SignIn = () => {
  const classes = cl();
  const dispatch = useDispatch();
  const history = useHistory();
  const [token, setToken] = React.useState('');
  const [passErr, setPassErr] = React.useState(false);
  const loading = GetRedux(KEY_LOADING_USER);
  const userInfo = GetRedux(KEY_DATA_USER);
  const actualUser = JSON.parse(sessionStorage.getItem('USER'));
  const [openToast, setOpenToast] = React.useState(false);
  const list = GetRedux(KEY_DATA_LIST);
  const onLogin = () => {
    const data = { token };
    const payload = { data, ...LOGIN_TO_SEND };
    dispatch(ActionSagas(LOGIN, payload));
  };
  React.useEffect(() => {
    if (!userInfo) {
      if (actualUser && actualUser.token) {
        dispatch(ActionAsync(LOGIN_SUCCESS, actualUser));
        if (actualUser.email !== '') {
          dispatch(ActionSagas(GET_LIST, LIST_TO_SEND));
        }
        history.push('/home');
      }
    } else if (userInfo.token) {
      sessionStorage.setItem('USER', JSON.stringify(userInfo));
      if (userInfo.email !== '') {
        dispatch(ActionSagas(GET_LIST, LIST_TO_SEND));
      }
      history.push('/home');
    }
  }, [userInfo]);
  React.useEffect(() => {
    if (userInfo && userInfo.code === 400) {
      setOpenToast(true);
    }
  });
  React.useEffect(() => {
    if (list && list.payload) {
      history.push('/home');
    }
  }, [list]);
  const [visible, setVisible] = React.useState(true);
  if (!loading) {
    return (
      <CustomContainer maxWidth="sm">
        <Toast open={openToast} message="invalid.secret.key" severity="error" />
        <CustomCard width="80%" height="500px" display="flex" padding="25px">
          <img alt="logo sign in" src={imgLogin} width={300} height={160} />
          {visible
          && (
          <>
            <div className={classes.titleAdmin}>
              <Font id="login" className="titleOption" />
            </div>
            <Input
              error={passErr}
              fullWidth
              label="secret.key"
              margin="normal"
              required
              variant="outlined"
              type="text"
              value={token || ''}
              onChange={(p) => setToken(p.target.value.trim())}
              onFocus={() => setPassErr(false)}
              onBlur={() => setPassErr(!(token.length > 3))}
              helperText={passErr
              && <Font id={token !== '' ? 'token.minimum' : 'field.require'} rest={{ color: 'error' }} />}
            />
            <ContentAlt>
              <PrincipalBtn disabled={token.length < 10} variant="contained" color="primary" onClick={onLogin}>
                <Font id="login" />
              </PrincipalBtn>
            </ContentAlt>
            <div className={classes.contentLink}>
              <LinkButton onClick={() => setVisible(false)}>
                <Font id="user.admin" />
              </LinkButton>
            </div>
          </>
          )}
          {
            !visible && <SigninAdmin setVisible={() => setVisible(true)} />
          }
        </CustomCard>
      </CustomContainer>
    );
  }
  return (
    <CustomContainer maxWidth="sm">
      <CustomCard width="80%" height="500px" display="flex" padding="25px">
        <img alt="logo sign in" src={imgLogin} width={300} height={160} />
        <Skeleton height={65} width="100%" />
        <div style={{ width: '100%' }}>
          <Skeleton height={65} width="35%" />
        </div>
      </CustomCard>
    </CustomContainer>
  );
};

export default SignIn;
