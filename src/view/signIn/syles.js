import { makeStyles } from '@material-ui/styles';

const cl = makeStyles(() => ({
  contentLink: {
    width: '100%',
    padding: 10,
    display: 'flex',
    justifyContent: 'flex-end',
  },
  titleAdmin: {
    width: '100%',
  },
}));
export default cl;
