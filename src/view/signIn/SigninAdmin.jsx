import React from 'react';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import { Visibility, VisibilityOff } from '@material-ui/icons';
import {
  ContentAlt, Font, Input, LinkButton, PrincipalBtn,
} from 'component';
import GetRedux from 'redux/selector/getSelector';
import { KEY_DATA_USER, KEY_LOADING_USER } from 'redux/selector/keys';
import { useDispatch } from 'react-redux';
import Skeleton from '@material-ui/lab/Skeleton';
import cl from './syles';
import { ActionSagas } from '../../redux/action';
import { LOGIN } from '../../redux/types';
import { LOGIN_TO_SEND } from '../../redux/api/axios/dataToSend';
import { LOGIN_URL_ADMIN } from '../../redux/api/axios/url';

const SigninAdmin = ({ setVisible }) => {
  const dispatch = useDispatch();
  const classes = cl();
  const [user, setUser] = React.useState('');
  const [isT, setIsT] = React.useState('');
  const [psw, setPsw] = React.useState('');
  const [ePsw, setEpsw] = React.useState(false);
  const [eUs, setEus] = React.useState(false);
  const regex = /^[A-Za-z0-9_.@!]{4,}$/;
  const disabled = user.length < 4 || !regex.test(user) || psw.length < 3;
  const loading = GetRedux(KEY_LOADING_USER);
  const userInfo = GetRedux(KEY_DATA_USER);
  const onLogin = () => {
    const data = { user, password: psw };
    const payload = { data, ...LOGIN_TO_SEND, url: LOGIN_URL_ADMIN };
    dispatch(ActionSagas(LOGIN, payload));
  };
  console.log(userInfo);
  if (!loading) {
    return (
      <>
        <div className={classes.titleAdmin}>
          <Font id="user.admin" className="titleOption" />
        </div>
        <Input
          error={eUs}
          fullWidth
          label="username"
          margin="normal"
          required
          variant="outlined"
          type="text"
          value={user || ''}
          onChange={(p) => setUser(p.target.value.trim())}
          onFocus={() => setEus(false)}
          onBlur={() => setEus(!(regex.test(user)))}
          helperText={eUs
                && <Font id={user !== '' ? 'user.admin.error.message' : 'field.require'} rest={{ color: 'error' }} />}
        />
        <Input
          error={eUs}
          fullWidth
          label="password"
          margin="normal"
          required
          variant="outlined"
          type={isT ? 'text' : 'password'}
          value={psw || ''}
          onChange={(p) => setPsw(p.target.value)}
          onFocus={() => setEpsw(false)}
          onBlur={() => setEpsw(psw.length < 3)}
          helperText={ePsw
                && <Font id={psw !== '' ? 'password.minimum' : 'field.require'} rest={{ color: 'error' }} />}
          InputProps={{
            endAdornment:
  <InputAdornment position="end">
    <IconButton
      aria-label="toogle password visibility"
      onClick={() => setIsT(!isT)}
    >
      {isT ? <Visibility /> : <VisibilityOff />}
    </IconButton>
  </InputAdornment>,
          }}
        />
        <ContentAlt>
          <PrincipalBtn disabled={disabled} variant="contained" color="primary" onClick={onLogin}>
            <Font id="login" />
          </PrincipalBtn>
        </ContentAlt>
        <div className={classes.contentLink}>
          <LinkButton onClick={setVisible}><Font id="login" /></LinkButton>
        </div>
      </>
    );
  }
  return (
    <>
      <Skeleton height={65} width="100%" />
      <Skeleton height={65} width="100%" />
      <div style={{ width: '100%' }}>
        <Skeleton height={65} width="35%" />
      </div>
    </>
  );
};

export default SigninAdmin;
