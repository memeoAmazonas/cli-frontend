import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import languages from './languages';

const resources = {
  en: {
    translation: {
      ...languages.en,
    },
  },
  es: {
    translation: {
      ...languages.es,
    },
  },
};
const localLang = localStorage.getItem('LANG');
i18n
  .use(initReactI18next)
  .init({
    resources,
    lng: localLang || 'es',
    keySeparator: false,
    interpolation: {
      escapeValue: false,
    },
  });
export default i18n;
