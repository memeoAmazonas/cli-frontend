# project-tool-gitlab

Herramienta para la generación de proyectos usando templates.

## Comenzando 🚀

### Pre-requisitos 📋
 * node version 12 o superior.
 
## Construido con 🛠️

* [ReactJS](https://reactjs.org/) - A JavaScript library for building user interfaces.
* [Redux](https://es.redux.js.org/) -A Predictable State Container for JS Apps.
* [Material-UI](https://material-ui.com/) -React components for faster and easier web development.

## Desarrollo
### configuracion
    En la raiz del proyecto se encuentra el archivo .dev, este posee las variables de entorno para la puesta en marcha, 
    para desarrollo no se recomienda cambiar solo en caso de producción, puede cambiar la url del servidor.  
### run
    Nos movemos a la raiz del proyecto, si es la primera vez que levantamos el proyecto ejecutamos el paso 1, sino se omite.
* paso 1:
```
npm install
```
* paso 2:
```
npm run start
Abre un navegador con el inicio del proyecto.
```

##Nota: 
    Para poder ver en funcionamiento tanto front como back deben estar corriendo.

## Despliegue 📦

### Compilación
```
npm run build
```
Genera una carpeta build con los archivos compilados.